# Quick Makefile to compile easily the poster, uses PDFLaTeX (pdflatex) and BibTeX (bibtex)
all: clean pdf evince clean

pdf:
	pdflatex herbier.tex
	pdflatex herbier.tex
	# bibtex herbier
	# pdflatex herbier.tex
	# pdflatex herbier.tex

evince:
	-evince herbier.pdf >/dev/null 2>/dev/null &

clean:
	-rm -rfv *.fls *.fdb_latexmk *.ps *.dvi *.htoc *.tms *.tid *.lg *.log *.id[vx] *.vrb *.toc *.snm *.nav *.htmp *.aux *.tmp *.out *.haux *.hidx *.bbl *.blg *.brf *.lof *.ilg *.ind *.meta *.fdb_latexmk *.fls *.synctex.gz*

bst:
	tex naereen.dbj
	mv -vf naereen.log /tmp/

# Download my script from https://bitbucket.org/lbesson/bin/src/master/PDFCompress
compress:
	PDFCompress herbier.pdf